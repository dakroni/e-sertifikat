<?php 
    require 'connection.php';
    $No = null;
    if(!empty($_GET['no']))
    {
        $No = $_GET['no'];
    }
    if($No == null)
    {
        header("Location: index.php");
    }
	if ( !empty($_POST))
    {
              
        // post values
        $no  = $_POST['no'];
        $nama_peserta  = $_POST['nama_peserta'];
        $nip_peserta  = $_POST['nip_peserta'];
        $instansi_peserta  = $_POST['instansi_peserta'];
        $dalamrangka  = $_POST['dalamrangka'];
        $tgl_tmp_pejabat  = $_POST['tgl_tmp_pejabat'];
        $jabatan  = $_POST['jabatan'];
        $nama_pejabat  = $_POST['nama_pejabat'];
        $nip_pejabat  = $_POST['nip_pejabat'];
        $no_sertifikat  = $_POST['no_sertifikat'];
		
		// Update data
        $query = 	"Update peserta set nama_peserta='$nama_peserta', nip_peserta='$nip_peserta', instansi_peserta='$instansi_peserta', 
					dalamrangka='$dalamrangka', tgl_tmp_pejabat='$tgl_tmp_pejabat', jabatan='$jabatan', nama_pejabat='$nama_pejabat', 
					nip_pejabat='$nip_pejabat',no_sertifikat='$no_sertifikat' WHERE no='$no'";
        mysqli_query($con,$query);
		 header("Location: index.php");
    }
	else
	{
		
		$query = "SELECT * FROM peserta where no = $No";
		$res    = mysqli_query($con,$query);
		
		$data=mysqli_fetch_array($res);
		
		
	
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="tutorial-boostrap-merubaha-warna">
	<meta name="author" content="ilmu-detil.blogspot.com">
	<title>SERTIFIKAT</title>
	<link rel="shortcut icon" href="logo.jpg">
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">	
	
	<style type="text/css">
	.navbar-default {
		background-color: #3b5998;
		font-size:18px;
		color:#ffffff;
	}
	
	</style>
</head>
<body>

<nav class="navbar navbar-dark bg-primary">
	<div class="container"> 
		<ul class="nav navbar-nav navbar-right">
        <li><a href="logout.php" class="btn btn-primary">Logout</a></li>        
      </ul>
	 </div>
</nav>
<!-- /.navbar -->
<div class="container">
    <div class="row">
        <div class="row">
            <h3>Update a User</h3>
			
        </div>
          <div class="panel-body">  
        <form method="POST" action="update.php">
		<div class="col-md-6">
			<div class="form-group">
				
				<input type="hidden" class="form-control" required="required" value="<?php echo $data['no'];?>" name="no">
				
			</div>
			<div class="form-group">
				<label for="NamaPeserta">Nama Peserta</label>
				<input type="text" class="form-control" required="required" value="<?php echo $data['nama_peserta'];?>" name="nama_peserta" placeholder="Nama Peserta">
				<span class="help-block"></span>
			</div>
			
			<div class="form-group">
				<label for="NIPPeserta">NIP Peserta</label>
				<input type="text" class="form-control" required="required" value="<?php echo $data['nip_peserta'];?>" name="nip_peserta" placeholder="NIP Peserta">
				<span class="help-block"></span>
			</div>		
			<div class="form-group">
				<label for="instansiPeserta">Instansi Peserta</label>
				<input type="text" class="form-control" required="required" value="<?php echo $data['instansi_peserta'];?>" name="instansi_peserta" placeholder="Instansi Peserta">
				<span class="help-block"></span>
			</div>
			<div class="form-group">
				<label for="dalamrangka">Dalam Rangka</label>
				<input type="text" class="form-control" required="required" value="<?php echo $data['dalamrangka'];?>" name="dalamrangka" placeholder="Dalam Rangka apa?">
				<span class="help-block"></span>
			</div>
			<div class="form-group">
				<label for="ttp">Tempat Tgl Pejabat</label>
				<input type="text" class="form-control" required="required" value="<?php echo $data['tgl_tmp_pejabat'];?>" name="tgl_tmp_pejabat" placeholder="Tanggal dan tempat di ttd pejabat">
				<span class="help-block"></span>
			</div>
			<div class="form-group">
				<label for="Jabatan">Jabatan Pejabat</label>
				<input type="text" class="form-control" required="required" value="<?php echo $data['jabatan'];?>" name="jabatan" placeholder="Jabatan Pejabat">
				<span class="help-block"></span>
			</div>
			<div class="form-group">
				<label for="NamaPejabat">Nama Pejabat</label>
				<input type="text" class="form-control" required="required"  value="<?php echo $data['nama_pejabat'];?>" name="nama_pejabat" placeholder="Nama Pejabat">
				<span class="help-block"></span>
			</div>
			<div class="form-group">
				<label for="NIPPejabat">NIP Pejabat</label>
				<input type="text" class="form-control" required="required" value="<?php echo $data['nip_pejabat'];?>" name="nip_pejabat" placeholder="NIP Pejabat">
				<span class="help-block"></span>
			</div>
			<div class="form-group">
				<label for="NoSertifikat">Nomor Sertifikat</label>
				<input type="text" class="form-control" required="required"  value="<?php echo $data['no_sertifikat'];?>" name="no_sertifikat" placeholder="No Sertifikat">
				<span class="help-block"></span>
			</div>
    
			<div class="form-actions">
				<button type="submit" class="btn btn-primary">Update</button>
				<a class="btn btn btn-default" href="index.php">Back</a>
			</div>
			
		</form>
		</div>
		</div>
                
    </div> <!-- /row -->
</div> <!-- /container -->
<?php } ?>
</body>
</html>
