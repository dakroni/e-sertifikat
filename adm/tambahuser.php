<?php 
    if ( !empty($_POST)) {
        require 'connection.php';
        // validation errors
        $nama     = null;
        $username     = null;
        $password       = null;
        
        // post values
        $nama  = $_POST['nama'];
        $username  = $_POST['username'];
        $password    = $_POST['password'];
        
        // validate input
        $valid = true;
        if(empty($nama)) {
            $fnameError = 'Please enter nama';
            $valid = false;
        }
        
        if(empty($username)) {
            $lnameError = 'Please enter username';
            $valid = false;
        }
        
        if(empty($password)) {
            $ageError = 'Please enter Password';
            $valid = false;
        }
        
        // insert data
        if ($valid) {
            
            mysqli_query($con,"INSERT INTO user VALUES('','$nama','$username','$password')");
			header("Location: index.php");
			
			
        }
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="tutorial-boostrap-merubaha-warna">
	<meta name="author" content="ilmu-detil.blogspot.com">
	<title>SERTIFIKAT</title>
	<link rel="shortcut icon" href="logo.jpg">
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">	
	
	<style type="text/css">
	.navbar-default {
		background-color: #3b5998;
		font-size:18px;
		color:#ffffff;
	}
	
	</style>
</head>
<body>

<nav class="navbar navbar-dark bg-primary">
	<div class="container">
  <!-- Navbar content -->
 
		<ul class="nav navbar-nav navbar-right">
        <li><a href="logout.php" class="btn btn-primary">Logout</a></li>        
		</ul>
		<ul class="nav navbar-nav navbar-right">
        <li><a href="../index.php" class="btn btn-primary">Halaman Peserta</a></li>        
		</ul>
		<ul class="nav navbar-nav navbar-right">
        <li><a href="tambahuser.php" class="btn btn-primary">Add Admin</a></li>        
		</ul>
		<ul class="nav navbar-nav navbar-left">
        <li><a href="import.php" class="btn btn-primary">IMPORT DATA CSV</a></li>
        
		</ul>
	 </div>
</nav>
<!-- /.navbar -->
<div class="container">
        <div class="row">
		<div class="col-sm-5 col-sm-offset-3"><h3>Tambah Pengguna Panitia</h3>
        <form method="POST" action="">
			<div class="form-group">
				<label for="nama">Nama</label>
				<input type="text" class="form-control" required="required" id="nama" name="nama" placeholder="Nama Lengkap">
				<span class="help-block"></span>
			</div>
			
			<div class="form-group ">
				<label for="username">USERNAME</label>
				<input type="text" class="form-control" required="required" id="username" name="username" placeholder="USERNAME">
        		<span class="help-block"></span>
			</div>
			
			<div class="form-group">
				<label for="inputAge">PASSWORD</label>
				<input type="password" required="required" class="form-control" id="password" name="password" placeholder="PASSWORD">
				<span class="help-block"></span>
			</div>	
			
				
			<div class="form-actions">
					<button type="submit" class="btn btn-success">Buat</button>
					<a class="btn btn btn-default" href="index.php">Kembali</a>
			</div>
		</form>
        </div></div>        
    <!-- /row -->
</div>
</body>
</html>
