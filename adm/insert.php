<?php 
    if ( !empty($_POST)) {
        require 'connection.php';
        // validation errors
        $fnameError     = null;
        $lnameError     = null;
        $ageError       = null;
        $genderError    = null;
        
        // post values
        $fname  = $_POST['fname'];
        $lname  = $_POST['lname'];
        $age    = $_POST['age'];
        $gender = $_POST['gender'];
        
        // validate input
        $valid = true;
        if(empty($fname)) {
            $fnameError = 'Please enter First Name';
            $valid = false;
        }
        
        if(empty($lname)) {
            $lnameError = 'Please enter Last Name';
            $valid = false;
        }
        
        if(empty($age)) {
            $ageError = 'Please enter Age';
            $valid = false;
        }
        
        if(empty($gender)) {
            $genderError = 'Please select Gender';
            $valid = false;
        }
        
        // insert data
        if ($valid) {
            
            mysqli_query($con,"INSERT INTO users Values('','$fname','$lname','$age','$gender')");
			header("Location: index.php");
			
			
        }
    }
?>


<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="tutorial-boostrap-merubaha-warna">
	<meta name="author" content="ilmu-detil.blogspot.com">
	<title>Tutorial Boostrap </title>
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	
	<style type="text/css">
	.navbar-default {
		background-color: #3b5998;
		font-size:18px;
		color:#ffffff;
	}
	</style>
</head>
<body>
<nav class="navbar navbar-default">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <h4><a href="https://ekonyontek23.blogspot.com" style='color:white;'target="_blank">EKO SUPRATNA</a></h4>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      
    </div>
  </div>
</nav>
<!-- /.navbar -->
<div class="container">
        <div class="row">
		<div class="col-sm-5 col-sm-offset-3"><h3>Tambah Data</h3>
        <form method="POST" action="">
			<div class="form-group">
				<label for="inputFName">First Name</label>
				<input type="text" class="form-control" required="required" id="inputFName" name="fname" placeholder="First Name">
				<span class="help-block"></span>
			</div>
			
			<div class="form-group ">
				<label for="inputLName">Last Name</label>
				<input type="text" class="form-control" required="required" id="inputLName" name="lname" placeholder="Last Name">
        		<span class="help-block"></span>
			</div>
			
			<div class="form-group">
				<label for="inputAge">Umur</label>
				<input type="number" required="required" class="form-control" id="inputAge" name="age" placeholder="Umur">
				<span class="help-block"></span>
			</div>
				<div class="form-group">
					<label for="inputGender">Jenis Kelamin</label>
					<select class="form-control" required="required" id="inputGender" name="Jenis Kelamin" >
						<option>Pilih Satu</option>
						<option value="male">Laki2</option>
						<option value="female">Perempuan</option>
					</select>
					<span class="help-block"></span>
        		</div>
    
			<div class="form-actions">
					<button type="submit" class="btn btn-success">Buat</button>
					<a class="btn btn btn-default" href="index.php">Kembali</a>
			</div>
		</form>
        </div></div>        
    <!-- /row -->
</div>
</body>
</html>
