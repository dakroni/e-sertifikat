<?php
	require 'connection.php';
    $no= null;
    if(!empty($_GET['no']))
    {
        $no = $_GET['no'];
    }
    if($no == null)
    {
        header("Location: index.php");
    } 
    if ( !empty($_POST))
    {
        
        // Delete Data
        $no = $_POST['no'];
       
        $query = "Delete from peserta where no=$no";
		mysqli_query($con,$query);
        header("Location: index.php");
    }
	$query = "SELECT * FROM peserta where no = $no";
		$res    = mysqli_query($con,$query);
		
		$data=mysqli_fetch_array($res);
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="tutorial-boostrap-merubaha-warna">
	<meta name="author" content="ilmu-detil.blogspot.com">
	<title>Tutorial Boostrap </title>
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	
	<style type="text/css">
	.navbar-default {
		background-color: #3b5998;
		font-size:18px;
		color:#ffffff;
	}
	</style>
</head>
<body>
<nav class="navbar navbar-default">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <h4>Detailed Technology Center</h4>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      
    </div>
  </div>
</nav>
<!-- /.navbar -->

<div class="container">
    <div class="row">
        <div class="row">
            <h3>Delete a User</h3>
        </div>
		<form method="POST" action="delete.php">
			<input type="hidden" name="no" value="<?php echo $no;?>" />
			<p class="bg-danger" style="padding: 10px;">Are you sure to delete <b>Nama&nbsp<?php echo $data['nama_peserta']; echo ', NIP:'; echo '&nbsp'; echo $data['nip_peserta'];?><b></p>
			<div class="form-actions">
				<button type="submit" class="btn btn-danger">Yes</button>
				<a class="btn btn btn-default" href="index.php">No</a>
			</div>
		</form>
                
    </div> <!-- /row -->
</div> 

</body>
</html>
