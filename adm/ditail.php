<?php 
    require 'connection.php';
    $No = null;
    if(!empty($_GET['no']))
    {
        $No = $_GET['no'];
    }
    if($No == null)
    {
        header("Location: index.php");
    }
    else
    {
        // read data
        
        $query 		= "SELECT * FROM peserta where no = $No";
		$result    	= mysqli_query($con,$query);
		$data		=mysqli_fetch_array($result);
       
    }
?>
<!DOCTYPE html>
<html>
<head>
<link rel="shortcut icon" href="logo.jpg">
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">	
    <title>Cetak Sertifikat</title>
</head>
<style>
*{margin: 0;padding: 0}
.box{
    width: 1200px;
    height: 100%;
    position: absolute;
}
.box img{
    width: 100%;
}
img,p{
    position: absolute;
}
.atasKiri{
    top: 20%;
    left: 20%;
}
.bawahKiri{
    bottom: 20%;
    left: 20%;
}
.atasKanan{
    top: 10%;
    right: 10%;
}
.atasTengah{
	text-align: center;
    left: 50%;
    top: 25%;
    transform: translate(-50%);
    -webkit-transform: translate(-50%);
    -moz-transform: translate(-50%);
    -o-transform: translate(-50%);
}
.menerangkan{
    left: 50%;
    top: 35%;
    transform: translate(-50%);
    -webkit-transform: translate(-50%);
    -moz-transform: translate(-50%);
    -o-transform: translate(-50%);
}
.namaPeserta{
    left: 50%;
    top: 38%;
    transform: translate(-50%);
    -webkit-transform: translate(-50%);
    -moz-transform: translate(-50%);
    -o-transform: translate(-50%);
}
.nipPeserta{
    left: 50%;
    top: 41%;
    transform: translate(-50%);
    -webkit-transform: translate(-50%);
    -moz-transform: translate(-50%);
    -o-transform: translate(-50%);
}
.instansiPeserta{
    left: 50%;
    top: 43%;
    transform: translate(-50%);
    -webkit-transform: translate(-50%);
    -moz-transform: translate(-50%);
    -o-transform: translate(-50%);
}
.sebagai{
    left: 50%;
    top: 48%;
    transform: translate(-50%);
    -webkit-transform: translate(-50%);
    -moz-transform: translate(-50%);
    -o-transform: translate(-50%);
}
.status{
	
    left: 50%;
    top: 52%;
    transform: translate(-50%);
    -webkit-transform: translate(-50%);
    -moz-transform: translate(-50%);
    -o-transform: translate(-50%);
}
.kegiatan{
	text-align: center;
    left: 50%;
    top: 46%;
    transform: translate(-50%);
    -webkit-transform: translate(-50%);
    -moz-transform: translate(-50%);
    -o-transform: translate(-50%);
}
.demikian{
	text-align: center;
    left: 50%;
    top: 62%;
    transform: translate(-50%);
    -webkit-transform: translate(-50%);
    -moz-transform: translate(-50%);
    -o-transform: translate(-50%);
}
.ttt{
	text-align: center;
    left: 50%;
    top: 69%;
    transform: translate(-50%);
    -webkit-transform: translate(-50%);
    -moz-transform: translate(-50%);
    -o-transform: translate(-50%);
}
.jabatan{
	text-align: center;
    left: 50%;
    top: 72%;
    transform: translate(-50%);
    -webkit-transform: translate(-50%);
    -moz-transform: translate(-50%);
    -o-transform: translate(-50%);
}
.namaPejabat{
    left: 50%;
    top: 80%;
    transform: translate(-50%);
    -webkit-transform: translate(-50%);
    -moz-transform: translate(-50%);
    -o-transform: translate(-50%);
}
.ttdstemple{
	padding-left: 42%;
	padding-top: 49%;
	
}


</style>

<body>
    <div class="box">
        <img src="sertifikat.png">       
        <p class="atasKanan">Nomor:<?php echo $data['no_sertifikat'];?></p>      
        <p class="atasTengah"></b>DIREKTORAT GURU DAN TENAGA KEPENDIDIKAN PENDIDIKAN DASAR<br>
								DIREKTORAT JENDERAL GURU DAN TENAGA KEPENDIDIKAN<br>
								KEMENTERIAN PENDIDIKAN DAN KEBUDAYAAN</b></p>
<p class="menerangkan">memberikan apresiasi kepada</p>
<p class="namaPeserta"><?php echo $data['nama_peserta'];?></p>
<p class="nipPeserta">NIP:<?php echo $data['nip_peserta'];?></p>
<p class="instansiPeserta"><?php echo $data['instansi_peserta'];?></p>

<p class="kegiatan">Sebagai <?php echo $data['status_peserta'];echo "&nbsp"; echo $data['dalamrangka'];?></b>
</p>
<p class="demikian">Demikian surat keterangan ini dibuat untuk dapat dipergunakan sebagaimana mestinya.</p>
<p class="ttt"><?php echo $data['tgl_tmp_pejabat'];?></p>

<p class="jabatan"><?php echo $data['jabatan'];?></p>

<p class="namaPejabat"><b><?php echo $data['nama_pejabat'];?></b><br>NIP.<?php echo $data['nip_pejabat'];?></p>
    </div>
	<img class="ttdstemple" src="ttd.png">
	<script>
        window.print();
    </script>   
</body>
</html>