<?php
session_start();
if (!isset($_SESSION['username'])) {
header("Location: index.php");
}
include 'connection.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="tutorial-boostrap-merubaha-warna">
	<meta name="author" content="ilmu-detil.blogspot.com">
	<title>SERTIFIKAT</title>
	<link rel="shortcut icon" href="logo.jpg">
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">	
	
	<style type="text/css">
	.navbar-default {
		background-color: #3b5998;
		font-size:18px;
		color:#ffffff;
	}
	
	</style>
</head>
<body>

<nav class="navbar navbar-dark bg-primary">
	<div class="container">
  <!-- Navbar content -->
 
		<ul class="nav navbar-nav navbar-right">
        <li><a href="logout.php" class="btn btn-primary">Logout</a></li>        
		</ul>
		<ul class="nav navbar-nav navbar-right">
        <li><a href="../index.php" class="btn btn-primary">Halaman Peserta</a></li>        
		</ul>
		<ul class="nav navbar-nav navbar-right">
        <li><a href="tambahuser.php" class="btn btn-primary">Add Admin</a></li>        
		</ul>
		<ul class="nav navbar-nav navbar-left">
        <li><a href="import.php" class="btn btn-primary">IMPORT DATA CSV</a></li>
        
		</ul>
	 </div>
</nav>

<div class="container">
<form class="form-group form-inline">			
	<div class="row">
	<div class="col-sm-6">
	<form class="form-inline" role="form" action="cari.php" method="get">
		<div class="form-group">
			<label>Cari :</label>
			<input type="text" name="cari" class="form-control" id="inputPassword2" placeholder="Nama pendek (satu kata)"> 						
		</div>
			<button type="submit" class="btn btn-primary mb-2">Cari</button>

	</form>				
	</div>	
	<div class="col-sm-6">				
		<form class="form-horizontal" action="" method="post" name="uploadCSV" enctype="multipart/form-data">
			<div class="input-row">
				<label>Choose CSV File</label> 
				<input type="file" name="file" id="file" accept=".csv" class="form-control">
				<button type="submit" id="submit" name="import" class="btn btn-primary mb-2">Import</button>
			</div>
		</form>			
	</div> 
</div>
</form>
<br>
<br>
<?php
if(isset($_GET['cari'])){
$cari = $_GET['cari'];
echo "<b>Hasil pencarian : ".$cari."</b>";
}
?>

	<div class ="row">
		<div class="col-md-12">
			<table class="table table-striped table-bordered table-hover">
			<thead>
				<tr>
					<th style="text-align:center">No Reg</th>
					<th style="text-align:center">Nama lengkap</th>
					<th style="text-align:center">Instansi</th>
					<th style="text-align:center">Kab/Kota</th>
					<th style="text-align:center">Kegiatan</th>
					<th style="text-align:center" colspan ="3">Action</th>
				</tr>
				</thead>
				<tbody>   
				<?php 
	  if(isset($_GET['cari'])){
	  $cari = $_GET['cari'];
	  $query  = "select * from peserta where nama_peserta like '%".$cari."%'";	      
	  $res1    = mysqli_query($con,$query);
	while($row=mysqli_fetch_array($res1)){		
		?>

  <tr>	
   <td><?php echo $row['no']; ?></td>
   <td><?php echo $row['nama_peserta']; ?></td>
   <td><?php echo $row['instansi_peserta']; ?></td>
   <td><?php echo $row['nip_peserta']; ?></td>
   <td><?php echo $row['dalamrangka']; ?></td>
    
	<td><div class="btn-group" role="group" aria-label="...">
  <a href="ditail.php?no=<?php echo $row['no'];?>" target="_blank"class="btn btn-success btn-xs">Cetak</a></button>
  <a href="update.php?no=<?php echo $row['no'];?>" class="btn btn-primary btn-xs">Edit</a></button>
  <a href="delete.php?no=<?php echo $row['no'];?>" class="btn btn-danger btn-xs">Delete</a></button>
	</div>   
   </td>
  <?php }
  
	}else{
	  $query2  = "select * from peserta ";
  
  $res    = mysqli_query($con,$query2);
  while($row=mysqli_fetch_array($res)){
  ?>
  <tr>	
   <td><?php echo $row['no']; ?></td>
   <td><?php echo $row['nama_peserta']; ?></td>
   <td><?php echo $row['instansi_peserta']; ?></td>
   <td><?php echo $row['nip_peserta']; ?></td>
   <td><?php echo $row['dalamrangka']; ?></td>
   <td>
   <div class="btn-group" role="group" aria-label="...">
  <a href="ditail.php?no=<?php echo $row['no'];?>" target="_blank"class="btn btn-success btn-xs">Cetak</a></button>
  <a href="update.php?no=<?php echo $row['no'];?>" class="btn btn-primary btn-xs">Edit</a></button>
  <a href="delete.php?no=<?php echo $row['no'];?>" class="btn btn-danger btn-xs">Delete</a></button>
	</div>
	</td>
  </tr>
   <div class="btn-group btn-group-sm" role="group">  
  
   
	<?php } } ?>
	
		 		</tbody>
			</table>
			<div class="form-group">
			<a class="btn btn-primary" href="cari.php" role="button">KEMBALI</a>
			</div>
		</div> 
	</div>
</div>    
</div>    
</body>
</html>
<script type="text/javascript">
	$(document).ready(
	function() {
		$("#frmCSVImport").on(
		"submit",
		function() {

			$("#response").attr("class", "");
			$("#response").html("");
			var fileType = ".csv";
			var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+("
					+ fileType + ")$");
			if (!regex.test($("#file").val().toLowerCase())) {
				$("#response").addClass("error");
				$("#response").addClass("display-block");
				$("#response").html(
						"Invalid File. Upload : <b>" + fileType
								+ "</b> Files.");
				return false;
			}
			return true;
		});
	});
</script>
<?php
include 'connection.php';

if (isset($_POST["import"])) {
    
    $fileName = $_FILES["file"]["tmp_name"];
    
    if ($_FILES["file"]["size"] > 0) {
        
        $file = fopen($fileName, "r");
        
        while (($column = fgetcsv($file, 10000, ";")) !== FALSE) {			
			
           $sqlInsert = "INSERT into peserta values ('".$column[0]."','".$column[1]."','".$column[2]."','".$column[3]."','".$column[4]."','".$column[5]."','".$column[6]."','".$column[7]."','".$column[8]."','".$column[9]."','".$column[10]."')";
            $result = mysqli_query($con, $sqlInsert);          
            if (! empty($result)) {
                $type = "success";
                $message = "CSV Data Imported into the Database";
            } else {
                $type = "error";
                $message = "Problem in Importing CSV Data";
            }
        }
    }
}
?>
